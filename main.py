import re
import json
import datetime
import asyncio
import aiohttp

class City_Weather():
    def __init__(self, city_name, city_temp, city_humidity, city_pressure,
                 wind, sunrise, sunset, day_length, weather_description):
        self.city_name = city_name
        self.city_temp = city_temp
        self.city_humidity = city_humidity
        self.city_pressure = city_pressure
        self.wind = wind
        self.sunrise = sunrise
        self.sunset = sunset
        self.day_length = day_length
        self.weather_description = weather_description

# Инициализируем лист для сохранения объектов City_Weather
cities_weather_list = []

# Bнициализируем список для наполнения городами
cities_list = []

# Откроем файл cities.txt и прочитаем его компоненты
with open('cities.txt', 'r', encoding='utf-8') as file:
    # Проитерируем каждую линию файла
    for line in file:
        # используем регулярные выражения для нахождения имени города на английском
        match = re.match(r'^\d+\) ([A-Za-z ]+)', line)
        if match:
            city_name = match.group(1).strip()  # Получим имя города и удалим пропуски спереди и сзади
            cities_list.append(city_name)

# Напечатем имена городов
print(cities_list)

openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'

async def fetch_weather_data(session, city):
    async with session.get(f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={openweatherAPI}&units=metric', ssl=False) as response:
        return await response.text()

async def get_weather_for_cities(cities_list):
    async with aiohttp.ClientSession() as session:
        tasks = [fetch_weather_data(session, city) for city in cities_list]
        results = await asyncio.gather(*tasks)
        return results

# Запустиv асинхронный цикл событий для получения данных о погоде для всех городов
async def main():
    weather_data_list = await get_weather_for_cities(cities_list)
    cities_weather_list = []

    for weather_data, city in zip(weather_data_list, cities_list):
        r = json.loads(weather_data)

        city_name = r['name']
        humidity = r['main']['humidity']
        pressure = r['main']['pressure']
        temp = r['main']['temp']
        wind = r['wind']['speed']
        sunrise = datetime.datetime.fromtimestamp(r['sys']['sunrise'])
        sunset = datetime.datetime.fromtimestamp(r['sys']['sunset'])
        day_length = sunset - sunrise
        weather_description = r['weather'][0]['main']

        city_weather = City_Weather(city_name, temp, humidity, pressure, wind, sunrise, sunset, day_length, weather_description)
        cities_weather_list.append(city_weather)

    # Теперь можно получить доступ к данным о погоде для каждого города, используя экземпляры City_Weather в списке cities_weather_list
    # Например, для печати сведений о погоде для каждого города:
    for city_weather in cities_weather_list:
        print(f'В городе {city_weather.city_name}\n'
              f'Температура воздуха составит: {city_weather.city_temp} C°\n'
              f'Влажность воздуха: {city_weather.city_humidity}%\n'
              f'Давление: {city_weather.city_pressure} мм.р.т.\n'
              f'Ветер: {city_weather.wind} м/с\n'
              f'Восход: {city_weather.sunrise}\n'
              f'Закат: {city_weather.sunset}\n'
              f'Продолжительность дня: {city_weather.day_length}\n'
              f'Описание погоды: {city_weather.weather_description}\n')

# Запуск асинхронного цикла событий
asyncio.run(main())
